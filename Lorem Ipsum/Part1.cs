using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Lorem_Ipsum
{
    [TestClass]
    public class Part1
    {
        IWebDriver driver = new ChromeDriver();

        [DataTestMethod]
        [DataRow("����")]
        public void WordCorrectlyAppearsInTheFirstParagraph(string checkWord)
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://lipsum.com/");

            HomePage homePage = new HomePage(driver);
            homePage.ClickOnRussianLangButton();
            var firstParagraphText = homePage.GetWhatIsLorIpsParagraphText();

            Assert.IsTrue(firstParagraphText.Contains(checkWord));

            driver.Quit();
        }

        [TestMethod]
        public void DefaultSettingResultInTextStartingWithLoremIpsum()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://lipsum.com/");

            HomePage homePage = new HomePage(driver);
            homePage.ClickOnGenerate();

            GeneratedLoremIpsumPage generatedLoremIpsumPage = new GeneratedLoremIpsumPage(driver);
            var firstGeneratedParagraphText = generatedLoremIpsumPage.GetFirstGeneratedParagraphText();

            Assert.IsTrue(firstGeneratedParagraphText.StartsWith("Lorem ipsum dolor sit amet, consectetur adipiscing elit"));

            driver.Quit();
        }
    }
}
