using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Lorem_Ipsum
{
    [TestClass]
    public class Part2
    {
        IWebDriver driver = new ChromeDriver();

        [DataTestMethod]
        [DataRow("-1")]
        [DataRow("0")]
        [DataRow("5")]
        [DataRow("10")]
        [DataRow("20")]
        public void CheckLoremIpsumIsGeneratedWithCorrectWordsAmount(string expectedWordsAmount)
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://lipsum.com/");

            HomePage homePage = new HomePage(driver);
            homePage.ClickOnWordsOptionCheckbox();
            homePage.ClearAmounOptionField();
            homePage.EnterAmountToOptionField(expectedWordsAmount);
            homePage.ClickOnGenerate();

            GeneratedLoremIpsumPage generatedLoremIpsumPage = new GeneratedLoremIpsumPage(driver);
            string actualAmount = generatedLoremIpsumPage.GetFirstGeneratedParagraphText().Trim(new char[] { ',', '.' }).Split(new char[] { ' ' }).Length.ToString();

            driver.Quit();
            Assert.AreEqual(expectedWordsAmount, actualAmount);
        }

        [DataTestMethod]
        [DataRow("-1")]
        [DataRow("0")]
        [DataRow("1")]
        [DataRow("3")]
        [DataRow("10")]
        public void CheckLoremIpsumIsGeneratedWithCorrectBytesAmount(string expectedBytesAmount)
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://lipsum.com/");

            HomePage homePage = new HomePage(driver);
            homePage.ClickOnBytesOptionCheckbox();
            homePage.ClearAmounOptionField();
            homePage.EnterAmountToOptionField(expectedBytesAmount);
            homePage.ClickOnGenerate();

            GeneratedLoremIpsumPage generatedLoremIpsumPage = new GeneratedLoremIpsumPage(driver);
            string actualAmount = generatedLoremIpsumPage.GetFirstGeneratedParagraphText().Length.ToString();

            driver.Quit();
            Assert.AreEqual(expectedBytesAmount, actualAmount);
        }

        [TestMethod]
        public void VerifyingStartWithLoremIpsumCheckbox()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://lipsum.com/");

            HomePage homePage = new HomePage(driver);
            homePage.ClickOnStartWithLorIpsCheckbox();
            homePage.ClickOnGenerate();

            GeneratedLoremIpsumPage generatedLoremIpsumPage = new GeneratedLoremIpsumPage(driver);
            string firstGeneratedParagraph = generatedLoremIpsumPage.GetFirstGeneratedParagraphText();
            
            driver.Quit();
            Assert.IsFalse(firstGeneratedParagraph.StartsWith("Lorem ipsum dolor sit amet, consectetur adipiscing elit"));
        }

        [TestMethod]
        public void CheckProbabilityOfLoremWord()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://lipsum.com/");

            int loremWordCounter = 0;
            int numberOfLaunches = 10;
                
            HomePage homePage = new HomePage(driver);
            

            for (int i = 0; i < numberOfLaunches; i++)
            {
                homePage.ClickOnGenerate();
                GeneratedLoremIpsumPage generatedLoremIpsumPage = new GeneratedLoremIpsumPage(driver);
                string firstGeneratedParagraph = generatedLoremIpsumPage.GetFirstGeneratedParagraphText();
                if (firstGeneratedParagraph.Contains("lorem"))
                {
                    ++loremWordCounter;
                }
                string secondGeneratedParagraph = generatedLoremIpsumPage.GetSecondGeneratedParagraphText();
                if (secondGeneratedParagraph.Contains("lorem"))
                {
                    ++loremWordCounter;
                }
                string thirdGeneratedParagraph = generatedLoremIpsumPage.GetThirdGeneratedParagraphText();
                if (thirdGeneratedParagraph.Contains("lorem"))
                {
                    ++loremWordCounter;
                }
                string fourthGeneratedParagraph = generatedLoremIpsumPage.GetFourthGeneratedParagraphText();
                if (fourthGeneratedParagraph.Contains("lorem"))
                {
                    ++loremWordCounter;
                }
                string fifthGeneratedParagraph = generatedLoremIpsumPage.GetFifthGeneratedParagraphText();
                if (fifthGeneratedParagraph.Contains("lorem"))
                {
                    ++loremWordCounter;
                }
                generatedLoremIpsumPage.ClickOnHomePageButton();
            }

            driver.Quit();
            Assert.IsTrue((loremWordCounter / numberOfLaunches) >= 2 && (loremWordCounter / numberOfLaunches) <= 3);
        }
        
    }
}
