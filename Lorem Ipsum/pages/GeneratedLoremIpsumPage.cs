﻿using OpenQA.Selenium;

namespace Lorem_Ipsum
{
    class GeneratedLoremIpsumPage
    {
        IWebDriver driver;

        By FirstParagraphOfGeneratedLoremIpsum = By.XPath("//div[contains(@id, 'lipsum')]/p[1]");
        By SecondParagraphOfGeneratedLoremIpsum = By.XPath("//div[contains(@id, 'lipsum')]/p[1]");
        By ThirdParagraphOfGeneratedLoremIpsum = By.XPath("//div[contains(@id, 'lipsum')]/p[1]");
        By FourthParagraphOfGeneratedLoremIpsum = By.XPath("//div[contains(@id, 'lipsum')]/p[1]");
        By FifthParagraphOfGeneratedLoremIpsum = By.XPath("//div[contains(@id, 'lipsum')]/p[1]");
        By HomePageButton = By.XPath("//a[contains(@href,'https://www.lipsum.com/')]");

        public GeneratedLoremIpsumPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public string GetFirstGeneratedParagraphText()
        {
            return driver.FindElement(FirstParagraphOfGeneratedLoremIpsum).Text;
        }

        public string GetSecondGeneratedParagraphText()
        {
            return driver.FindElement(SecondParagraphOfGeneratedLoremIpsum).Text;
        }

        public string GetThirdGeneratedParagraphText()
        {
            return driver.FindElement(ThirdParagraphOfGeneratedLoremIpsum).Text;
        }

        public string GetFourthGeneratedParagraphText()
        {
            return driver.FindElement(FourthParagraphOfGeneratedLoremIpsum).Text;
        }

        public string GetFifthGeneratedParagraphText()
        {
            return driver.FindElement(FifthParagraphOfGeneratedLoremIpsum).Text;
        }

        public void ClickOnHomePageButton()
        {
            driver.FindElement(HomePageButton).Click();
        }
    }
}
