﻿using OpenQA.Selenium;

namespace Lorem_Ipsum
{
    public class HomePage
    {
        IWebDriver driver;

        By RussianLanguageButton = By.XPath("//div[contains(@id, 'Languages')]//a[contains(text(), 'Pyccкий')]");
        By WhatIsLorIpsParagraph = By.XPath("//strong[text()='Lorem Ipsum']/parent::p");
        By StartWithLorIpsCheckbox = By.XPath("//input[contains(@type, 'checkbox') and contains(@name, 'start')]");
        By GenerateLoremIpsumButton = By.XPath("//input[contains(@type, 'submit')]");
        By ParagraphsOptionCheckbox = By.XPath("//input[contains(@id, 'paras')]");
        By WordsOptionCheckbox = By.XPath("//input[contains(@id, 'words')]");
        By BytesOptionCheckbox = By.XPath("//input[contains(@id, 'bytes')]");
        By ListsOptionCheckbox = By.XPath("//input[contains(@id, 'lists')]");
        By AmountOptionField = By.XPath("//input[contains(@id, 'amount')]");

        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
        }
        
        public string GetWhatIsLorIpsParagraphText()
        {
            return driver.FindElement(WhatIsLorIpsParagraph).Text;
        }

        public void ClickOnRussianLangButton()
        {
            driver.FindElement(RussianLanguageButton).Click();
        }

        public void ClickOnGenerate()
        {
            driver.FindElement(GenerateLoremIpsumButton).Click();
        }

        public void ClickOnStartWithLorIpsCheckbox()
        {
            driver.FindElement(StartWithLorIpsCheckbox).Click();
        }
        public void ClickOnParagraphsOptionCheckbox()
        {
            driver.FindElement(ParagraphsOptionCheckbox).Click();
        }

        public void ClickOnWordsOptionCheckbox()
        {
            driver.FindElement(WordsOptionCheckbox).Click();
        }

        public void ClickOnBytesOptionCheckbox()
        {
            driver.FindElement(BytesOptionCheckbox).Click();
        }
        public void ClickOnListsOptionCheckbox()
        {
            driver.FindElement(ListsOptionCheckbox).Click();
        }

        public void ClearAmounOptionField()
        {
            driver.FindElement(AmountOptionField).Clear();
        }

        public void EnterAmountToOptionField(string amount)
        {
            driver.FindElement(AmountOptionField).SendKeys(amount);
        }

    }
}
